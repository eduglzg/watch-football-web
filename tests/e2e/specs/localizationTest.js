describe('Change locale', () => {
  it('Successfull locale change', () => {
    cy.visit('/')
    cy.get('[data-cy="localization-open"]').click()
    cy.get('[data-cy="localization-text"]')
      .should('be.visible')
      .should('contain', 'Translations')
    cy.get('[data-cy="localization-language"]').contains('Español').click()
    cy.get('[data-cy="localization-open"]').click()
    cy.get('[data-cy="localization-text"]')
      .should('be.visible')
      .should('contain', 'Traducciones')
      cy.get('[value="es"] > .v-btn').should('be.disabled')
    cy.get('[data-cy="localization-language"]').contains('English').click()
    cy.get('[data-cy="localization-open"]').click()
    cy.get('[data-cy="localization-text"]')
      .should('be.visible')
      .should('contain', 'Translations')
  })
})