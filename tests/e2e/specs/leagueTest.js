describe('League adding', () => {
  it('Button not showing when not logged', () => {
    cy.visit('/leagues')
    cy.get('.league-grid > .league-card:nth-child(1)').click()
    cy.get('[data-cy="league-addLeague"]').should('not.be.visible')
  })

  it('Button showing on login', () => {
    cy.login('eduardo.gonzalezg@alumnos.uva.es', '12345678')
    cy.visit('/leagues')
    cy.get('.league-grid > .league-card:nth-child(1)').click()
    cy.get('[data-cy="league-addLeague"]').should('be.visible')
  })

  it('Successfull league adding', () => {
    cy.visit('/leagues')
    cy.get('.league-grid > .league-card:nth-child(1)').click()
    cy.get('[data-cy="league-addLeague"]')
      .should('be.visible')
      .click()
    cy.wait(1000)
    cy.goToUser()
    cy.get('.user-statistics').should('contain', '1 League')
  })

  it('Successfull league removing', () => {
    cy.visit('/leagues')
    cy.get('.league-grid > .league-card:nth-child(1)').click()
    cy.get('[data-cy="league-removeLeague"]')
      .should('be.visible')
      .click()
    cy.wait(1000)
    cy.goToUser()
    cy.get('.user-statistics').should('contain', '0 Leagues')
    cy.logout()
  })
})