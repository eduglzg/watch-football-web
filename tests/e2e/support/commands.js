// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
Cypress.Commands.add("login", (email, password) => {
  cy.visit('/signIn')
  cy.get('[data-cy="login-email"]').type(email)
  cy.get('[data-cy="login-password"]').type(password)
  cy.get('[data-cy="login-button"]').click()
  cy.wait(4000)
})
Cypress.Commands.add("logout", () => {
  cy.visit('/')
  cy.get('[data-cy="AvatarMenu"]').click()
  cy.get('[data-cy="AvatarMenu-card"]').should('be.visible')
  cy.get('[data-cy="AvatarMenu-card-logout"]').click()
})
Cypress.Commands.add("goToUser", () => {
  cy.visit('/')
  cy.get('[data-cy="AvatarMenu"]').click()
  cy.get('[data-cy="AvatarMenu-card"]').should('be.visible')
  
  cy.get('[data-cy="AvatarMenu-card-button"]').contains('Profile').click()
})
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
