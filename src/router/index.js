import Vue from 'vue'
import VueRouter from 'vue-router'
import { auth } from '../plugins/firebase'

import Home from '../views/Home.vue'
import SignIn from '../views/SignIn.vue'
import SignUp from '../views/SignUp.vue'
import ComponentTesting from '../views/ComponentTesting.vue'

// Countries
import Countries from '../views/football/countries/Countries.vue'
import Country from '../views/football/countries/Country.vue'

// Leagues
import Leagues from '../views/football/leagues/Leagues.vue'
import League from '../views/football/leagues/League.vue'

// Fixtures
import Fixtures from '../views/football/fixtures/Fixtures.vue'
import Fixture from '../views/football/fixtures/Fixture.vue'

// Players
import Players from '../views/football/players/Players.vue'
import Player from '../views/football/players/Player.vue'

// Coachs
import Coachs from '../views/football/coachs/Coachs.vue'
import Coach from '../views/football/coachs/Coach.vue'

// Teams
import Teams from '../views/football/teams/Teams.vue'
import Team from '../views/football/teams/Team.vue'

// Users
import User from '../views/user/User.vue'
import EditUser from '../views/user/EditUser.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '*',
    redirect: '/signIn'
  },
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/results',
    name: 'Results',
    component: ComponentTesting
  },
  {
    path: '/leagues',
    name: 'Leagues',
    component: Leagues
  },
  {
    path: '/leagues/:leagueId',
    name: 'League',
    component: League
  },
  {
    path: '/signIn',
    name: 'SignIn',
    component: SignIn,
    meta: {
      requiresNoAuth: true
    }
  },
  {
    path: '/signUp',
    name: 'SignUp',
    component: SignUp,
    meta: {
      requiresNoAuth: true
    }
  },
  {
    path: '/fixtures',
    name: 'Fixtures',
    component: Fixtures
  },
  {
    path: '/fixtures/:id',
    component: Fixture
  },
  {
    path: '/players',
    name: 'Players',
    component: Players
  },
  {
    path: '/players/:playerId/:season',
    component: Player
  },
  {
    path: '/coachs',
    name: 'Coachs',
    component: Coachs
  },
  {
    path: '/coachs/:coachId',
    component: Coach
  },
  {
    path: '/teams',
    name: 'Teams',
    component: Teams
  },
  {
    path: '/teams/:teamId',
    name: 'Team',
    component: Team
  },
  {
    path: '/countries',
    name: 'Countries',
    component: Countries
  },
  {
    path: '/countries/:countryId',
    name: 'Country',
    component: Country
  },
  {
    path: '/user/:userId',
    name: 'User',
    component: User
  },
  {
    path: '/user/edit/:userId',
    name: 'EditUser',
    component: EditUser,
    meta: {
      requiresAuth: true
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

router.beforeEach(async (to, from, next) => {
  const usuario = auth.currentUser
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth)
  const requiresNoAuth = to.matched.some(record => record.meta.requiresNoAuth)

  if (requiresAuth && !usuario) {
    next({ name: 'SignIn' })
  } else if (requiresNoAuth && usuario) {
    next({ name: 'Home' })
  } else {
    next()
  }
})

export default router
