/* eslint-disable */
import SignUpCard from '../components/userAccess/SignUpCard.vue'

// 👇 This default export determines where your story goes in the story list
export default {
  title: 'SignUpCardStorie',
  component: SignUpCard
}

// 👇 We create a “template” of how args map to rendering
const Template = (args, { argTypes }) => ({
  components: { SignUpCard },
  props: Object.keys(argTypes),
  template: '<SignUpCard v-bind="$props" />'
})

export const FirstStory = Template.bind({})

FirstStory.args = { right:true
  /* 👇 The args you need here will depend on your component */

}