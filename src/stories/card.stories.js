/* eslint-disable */
import HorizontalCard from '../components/utils/HorizontalCard.vue'

// 👇 This default export determines where your story goes in the story list
export default {
  title: 'HorizontalCard',
  component: HorizontalCard
}

// 👇 We create a “template” of how args map to rendering
const Template = (args, { argTypes }) => ({
  components: { HorizontalCard },
  props: Object.keys(argTypes),
  template: '<HorizontalCard v-bind="$props" />'
})

export const FirstStory = Template.bind({})

FirstStory.args = { right:true
  /* 👇 The args you need here will depend on your component */

}
