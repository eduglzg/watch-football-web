import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'
import config from './firebaseConfig'

firebase.initializeApp(config)
// firebase.initializeApp(firebaseConfig);

if (process.env.NODE_ENV !== 'test') {
  firebase.auth().setPersistence(firebase.auth.Auth.Persistence.SESSION)
}

const db = firebase.firestore()
const auth = firebase.auth()
auth.setPersistence(firebase.auth.Auth.Persistence.SESSION)
const currentUser = auth.currentUser

// Firebase collections
const usersCollection = db.collection('users')

export {
  db,
  auth,
  currentUser,
  usersCollection
}
