import { auth, usersCollection } from '../../plugins/firebase'

const state = {
  user: {
    name: '',
    userName: '',
    email: '',
    country: '',
    uid: '',
    leagues: [],
    teams: [],
    fixtures: []
  }
}

const actions = {
  signUp ({ dispatch, commit }, data) {
    return new Promise((resolve, reject) => {
      auth.createUserWithEmailAndPassword(data.email, data.password)
        .then(res => {
          dispatch('sendVerification')
          commit('SET_EMAIL', res.user.email)
          return dispatch('setUserData', data)
        })
        .then(() => {
          resolve()
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  sendVerification () {
    auth.currentUser.sendEmailVerification()
  },
  sendPasswordResetEmail ({ commit }, email) {
    return auth.sendPasswordResetEmail(email)
  },
  setUserData ({ commit }, data) {
    return new Promise((resolve, reject) => {
      const userInfo = {
        name: data.name,
        userName: data.userName,
        email: data.email,
        country: data.country
      }
      usersCollection.doc(auth.currentUser.uid).set(userInfo)
        .then(() => {
          commit('SET_NAME', data.name)
          commit('SET_USERNAME', data.userName)
          commit('SET_EMAIL', data.email)
          commit('SET_COUNTRY', data.country)
          commit('SET_UID', auth.currentUser.uid)
          resolve()
        })
        .catch((error) => {
          console.error(error)
          reject(new Error('something bad happened'))
        })
    })
  },
  getUserData ({ dispatch, commit }) {
    if (auth.currentUser) {
      return new Promise((resolve, reject) => {
        usersCollection.doc(auth.currentUser.uid).get()
          .then(doc => {
            const data = doc.data()
            commit('SET_NAME', data.name)
            commit('SET_USERNAME', data.userName)
            commit('SET_EMAIL', data.email)
            commit('SET_COUNTRY', data.country)
            commit('SET_UID', auth.currentUser.uid)
            dispatch('getLeagues')
            dispatch('getTeams')
            dispatch('getFixtures')
            resolve(true)
          })
          .catch(() => {
            reject(new Error('something bad happened'))
          })
      })
    } else {
      return new Promise(resolve => resolve(false))
    }
  },
  signIn ({ dispatch }, data) {
    return new Promise((resolve, reject) => {
      auth.signInWithEmailAndPassword(data.email, data.password)
        .then(() => {
          return dispatch('getUserData')
        })
        .then(() => {
          resolve()
        })
        .catch((error) => {
          console.error(`${error.code}: ${error.message}`)
          reject(error)
        })
    })
  },
  signOut ({ commit }) {
    return new Promise((resolve, reject) => {
      auth.signOut()
        .then(() => {
          commit('CLEAR_USER')
          resolve()
        })
        .catch((error) => {
          console.error(error)
          reject(error)
        })
    })
  },

  // Leagues

  getLeagues ({ commit }) {
    if (auth.currentUser) {
      return new Promise((resolve, reject) => {
        usersCollection.doc(auth.currentUser.uid).collection('leagues').get()
          .then(querySnapshot => {
            const leagues = querySnapshot.docs.map(doc => { return { docId: doc.id, ...doc.data() } })
            commit('SET_LEAGUES', leagues)
            resolve(true)
          })
          .catch((error) => {
            console.error(error)
            reject(error)
          })
      })
    } else {
      return new Promise(resolve => resolve(false))
    }
  },
  addLeague ({ commit }, league) {
    return new Promise((resolve, reject) => {
      const leagueInfo = {
        league: league.league,
        country: league.country
      }
      usersCollection.doc(auth.currentUser.uid).collection('leagues').add(leagueInfo)
        .then((docRef) => {
          leagueInfo.docId = docRef.id
          commit('ADD_LEAGUE', leagueInfo)
          resolve()
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  removeLeague ({ commit }, league) {
    return new Promise((resolve, reject) => {
      const leagueInfo = {
        docId: league.docId
      }
      usersCollection.doc(auth.currentUser.uid).collection('leagues').doc(leagueInfo.docId).delete()
        .then(() => {
          commit('REMOVE_LEAGUE', leagueInfo)
          resolve()
        })
        .catch((error) => {
          console.error(error)
          reject(new Error('something bad happened'))
        })
    })
  },

  // Teams

  getTeams ({ commit }) {
    if (auth.currentUser) {
      return new Promise((resolve, reject) => {
        usersCollection.doc(auth.currentUser.uid).collection('teams').get()
          .then(querySnapshot => {
            const teams = querySnapshot.docs.map(doc => { return { docId: doc.id, ...doc.data() } })
            commit('SET_TEAMS', teams)
            resolve(true)
          })
          .catch((error) => {
            console.error(error)
            reject(error)
          })
      })
    } else {
      return new Promise(resolve => resolve(false))
    }
  },
  addTeam ({ commit }, team) {
    return new Promise((resolve, reject) => {
      const teamInfo = {
        team: team.team
      }
      usersCollection.doc(auth.currentUser.uid).collection('teams').add(teamInfo)
        .then((docRef) => {
          teamInfo.docId = docRef.id
          commit('ADD_TEAM', teamInfo)
          resolve()
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  removeTeam ({ commit }, team) {
    return new Promise((resolve, reject) => {
      const teamInfo = {
        docId: team.docId
      }
      usersCollection.doc(auth.currentUser.uid).collection('teams').doc(teamInfo.docId).delete()
        .then(() => {
          commit('REMOVE_TEAM', teamInfo)
          resolve()
        })
        .catch((error) => {
          reject(error)
        })
    })
  },

  // Fixtures

  getFixtures ({ commit }) {
    if (auth.currentUser) {
      return new Promise((resolve, reject) => {
        usersCollection.doc(auth.currentUser.uid).collection('fixtures').get()
          .then(querySnapshot => {
            const fixtures = querySnapshot.docs.map(doc => { return { docId: doc.id, ...doc.data() } })
            commit('SET_FIXTURES', fixtures)
            resolve(true)
          })
          .catch((error) => {
            console.error(error)
            reject(error)
          })
      })
    } else {
      return new Promise(resolve => resolve(false))
    }
  },
  addFixture ({ commit }, fixture) {
    return new Promise((resolve, reject) => {
      const fixtureInfo = {
        fixture: fixture.fixture,
        goals: fixture.goals,
        league: fixture.league,
        teams: fixture.teams
      }
      usersCollection.doc(auth.currentUser.uid).collection('fixtures').add(fixtureInfo)
        .then((docRef) => {
          fixtureInfo.docId = docRef.id
          commit('ADD_FIXTURE', fixtureInfo)
          resolve()
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  removeFixture ({ commit }, fixture) {
    return new Promise((resolve, reject) => {
      const fixtureInfo = {
        docId: fixture.docId
      }
      usersCollection.doc(auth.currentUser.uid).collection('fixtures').doc(fixtureInfo.docId).delete()
        .then(() => {
          commit('REMOVE_FIXTURE', fixtureInfo)
          resolve()
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  editUser ({ commit }, data) {
    return new Promise((resolve, reject) => {
      const userInfo = {
        name: data.name,
        userName: data.userName,
        country: data.country
      }
      usersCollection.doc(auth.currentUser.uid).update(userInfo)
        .then(() => {
          commit('SET_NAME', data.name)
          commit('SET_USERNAME', data.userName)
          commit('SET_COUNTRY', data.country)
          resolve()
        })
        .catch((error) => {
          console.error(error)
          reject(error)
        })
    })
  },
  editEmail ({ commit }, email) {
    return new Promise((resolve, reject) => {
      const user = auth.currentUser

      user.updateEmail(email).then(() => {
        usersCollection.doc(auth.currentUser.uid).update({ email: email })
        commit('SET_EMAIL', email)
        resolve()
      }).catch((error) => {
        console.error(error)
        reject(error)
      })
    })
  },
  editPassword ({ commit }, newPassword) {
    return new Promise((resolve, reject) => {
      const user = auth.currentUser

      user.updatePassword(newPassword).then(() => {
        resolve()
      }).catch((error) => {
        console.error(error)
        reject(error)
      })
    })
  }
}

const mutations = {
  SET_NAME (state, name) {
    state.user.name = name
  },
  SET_USERNAME (state, userName) {
    state.user.userName = userName
  },
  SET_EMAIL (state, email) {
    state.user.email = email
  },
  SET_COUNTRY (state, country) {
    state.user.country = country
  },
  SET_UID (state, uid) {
    state.user.uid = uid
  },
  SET_LEAGUES (state, leagues) {
    state.user.leagues = [...leagues]
  },
  ADD_LEAGUE (state, league) {
    state.user.leagues.push(league)
  },
  REMOVE_LEAGUE (state, league) {
    state.user.leagues.splice(state.user.leagues.findIndex(v => v.docId === league.docId), 1)
  },
  SET_TEAMS (state, teams) {
    state.user.teams = [...teams]
  },
  ADD_TEAM (state, team) {
    state.user.teams.push(team)
  },
  REMOVE_TEAM (state, team) {
    state.user.teams.splice(state.user.teams.findIndex(v => v.docId === team.docId), 1)
  },
  SET_FIXTURES (state, fixtures) {
    state.user.fixtures = [...fixtures]
  },
  ADD_FIXTURE (state, fixture) {
    state.user.fixtures.push(fixture)
  },
  REMOVE_FIXTURE (state, fixture) {
    state.user.fixtures.splice(state.user.fixtures.findIndex(v => v.docId === fixture.docId), 1)
  },
  CLEAR_USER (state) {
    state.user.name = ''
    state.user.userName = ''
    state.user.email = ''
    state.user.country = ''
    state.user.uid = ''
    state.user.leagues = []
    state.user.teams = []
    state.user.fixtures = []
  }
}

export default {
  state,
  mutations,
  actions
}
