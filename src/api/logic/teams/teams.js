import { axiosInstance } from '../../axiosDefault'

export default class TeamsService {
  getTeamById (id) {
    return axiosInstance.get('/teams', { params: { id: id } })
  }

  getTeamsByLeague (league, season) {
    return axiosInstance.get('/teams', { params: { league: league, season: season } })
  }

  getTeamsByCountry (country) {
    return axiosInstance.get('/teams', { params: { country: country } })
  }

  getTeamSeasons (id) {
    return axiosInstance.get('/teams/seasons', { params: { team: id } })
  }

  getTeamStatistics (team, league, season) {
    return axiosInstance.get('/teams/statistics', { params: { team: team, league: league, season: season } })
  }
}
