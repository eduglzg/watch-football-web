import { axiosInstance } from '../../axiosDefault'

export default class LeaguesService {
  getAllCurrentLeagues () {
    return axiosInstance.get('/leagues', { params: { current: true } })
  }

  getLeagueById (id) {
    return axiosInstance.get('/leagues', { params: { id: id } })
  }

  getLeagueByTeam (team) {
    return axiosInstance.get('/leagues', { params: { team: team } })
  }

  getLeaguesByCountryName (country) {
    return axiosInstance.get('/leagues', { params: { country: country } })
  }

  getLeaguesByCountryCode (code) {
    return axiosInstance.get('/leagues', { params: { code: code } })
  }
}
