import { axiosInstance } from '../../axiosDefault'

export default class CoachsService {
  getCoachById (id) {
    return axiosInstance.get('/coachs', { params: { id: id } })
  }

  getCoachsByTeam (team) {
    return axiosInstance.get('/coachs', { params: { team: team } })
  }
}
