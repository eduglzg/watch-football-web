import { axiosInstance } from '../../axiosDefault'

export default class CountriesService {
  getCountries () {
    return axiosInstance.get('/countries')
  }

  getCountriesByName (name) {
    return axiosInstance.get('/countries', { params: { name: name } })
  }

  getCountriesByCode (code) {
    return axiosInstance.get('/countries', { params: { code: code } })
  }

  searchCountries (name) {
    return axiosInstance.get('/countries', { params: { search: name } })
  }
}
