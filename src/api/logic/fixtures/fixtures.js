import { axiosInstance } from '../../axiosDefault'

export default class FixturesService {
  getFixtureById (id) {
    return axiosInstance.get('/fixtures', { params: { id: id } })
  }

  getFixturesByLeague (league, season) {
    return axiosInstance.get('/fixtures', { params: { league: league, season: season } })
  }

  getFixturesByTeam (team, season) {
    return axiosInstance.get('/fixtures', { params: { team: team, season: season } })
  }

  getFixturesByLive () {
    return axiosInstance.get('/fixtures', { params: { live: 'all' } })
  }
}
