import { axiosInstance } from '../../axiosDefault'

export default class Head2HeadService {
  getHead2HeadId (id1, id2) {
    return axiosInstance.get('fixtures/headtohead', { params: { h2h: id1 + '-' + id2 } })
  }
}
