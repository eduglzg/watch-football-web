import { axiosInstance } from '../../axiosDefault'

export default class LineupsService {
  getLineupId (fixtureId) {
    return axiosInstance.get('/fixtures/lineups', { params: { fixture: fixtureId } })
  }
}
