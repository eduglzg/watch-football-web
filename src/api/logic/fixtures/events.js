import { axiosInstance } from '../../axiosDefault'

export default class EventsService {
  getEventId (fixtureId) {
    return axiosInstance.get('/fixtures/events', { params: { fixture: fixtureId } })
  }
}
