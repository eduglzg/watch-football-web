import { axiosInstance } from '../../axiosDefault'

export default class PlayersService {
  getPlayerById (id, season) {
    return axiosInstance.get('/players', { params: { id: id, season: season } })
  }

  getPlayersByTeam (team, season) {
    return axiosInstance.get('/players', { params: { team: team, season: season } })
  }

  getTopScorers (league, season) {
    return axiosInstance.get('/players/topscorers', { params: { league: league, season: season } })
  }

  getTopAssists (league, season) {
    return axiosInstance.get('/players/topassists', { params: { league: league, season: season } })
  }

  getTopYellowCards (league, season) {
    return axiosInstance.get('/players/topyellowcards', { params: { league: league, season: season } })
  }

  getTopRedCards (league, season) {
    return axiosInstance.get('/players/topredcards', { params: { league: league, season: season } })
  }
}
