import { axiosInstance } from '../../axiosDefault'

export default class TransfersService {
  getTransfersByPlayer (player) {
    return axiosInstance.get('/transfers', { params: { player: player } })
  }

  getTransfersByTeam (team) {
    return axiosInstance.get('/transfers', { params: { team: team } })
  }
}
