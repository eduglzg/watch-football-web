import { axiosInstance } from '../../axiosDefault'

export default class StandingsService {
  getStandingsByLeague (league, season) {
    return axiosInstance.get('/standings', { params: { league: league, season: season } })
  }

  getStandingsByTeam (team, season) {
    return axiosInstance.get('/standings', { params: { team: team, season: season } })
  }
}
