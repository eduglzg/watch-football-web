import { axiosInstance } from '../../axiosDefault'

export default class SidelinedService {
  getSidelinedByPlayer (player) {
    return axiosInstance.get('/sidelined', { params: { player: player } })
  }

  getSidelinedByCoach (coach) {
    return axiosInstance.get('/sidelined', { params: { coach: coach } })
  }
}
