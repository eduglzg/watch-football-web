import { axiosInstance } from '../../axiosDefault'

export default class TrophiesService {
  getTrophiesByPlayer (player) {
    return axiosInstance.get('/trophies', { params: { player: player } })
  }

  getTrophiesByCoach (coach) {
    return axiosInstance.get('/trophies', { params: { coach: coach } })
  }
}
