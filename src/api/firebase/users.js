
import { usersCollection } from '~@/plugins/firebase'

export default class userService{
  getUserInfo(uid) {
    const docRef = usersCollection.doc(uid);
    return docRef.get();
  }
}