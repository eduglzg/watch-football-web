module.exports = [
  {
    path: '/leagues',
    method: 'GET',
    template: require('./responses/leagues.json')
  },
  {
    path: '/fixtures',
    method: 'GET',
    template: require('./responses/fixtures.json')
  },
  {
    path: '/fixtures/headtohead',
    method: 'GET',
    template: require('./responses/headtohead.json')
  },
  {
    path: '/coachs',
    method: 'GET',
    template: require('./responses/coachs.json')
  },
  {
    path: '/players',
    method: 'GET',
    template: require('./responses/players.json')
  },
  {
    path: '/transfers',
    method: 'GET',
    template: require('./responses/transfers.json')
  },
  {
    path: '/sidelined',
    method: 'GET',
    template: require('./responses/sidelined.json')
  },
  {
    path: '/standings',
    method: 'GET',
    template: require('./responses/standings.json')
  },
  {
    path: '/teams',
    method: 'GET',
    template: require('./responses/teams.json')
  },
  {
    path: '/teams/seasons',
    method: 'GET',
    template: require('./responses/seasons.json')
  },
  {
    path: '/trophies',
    method: 'GET',
    template: require('./responses/trophies.json')
  },
  {
    path: '/players/topscorers',
    method: 'GET',
    template: require('./responses/topScorers.json')
  },
  {
    path: '/players/topassists',
    method: 'GET',
    template: require('./responses/topAssists.json')
  },
  {
    path: '/players/topyellowcards',
    method: 'GET',
    template: require('./responses/topYellowCards.json')
  },
  {
    path: '/players/topredcards',
    method: 'GET',
    template: require('./responses/topRedCards.json')
  },
  {
    path: '/teams/statistics',
    method: 'GET',
    template: require('./responses/teamStatistics.json')
  },
  {
    path: '/countries',
    method: 'GET',
    template: require('./responses/countries.json')
  },
]